'use strict';

//tennety
//joebo
angular.module('demo', []).
  config(['$routeProvider', function($routeProvider) {
  $routeProvider.
      when('/', {templateUrl: 'partials/index.html',   controller: ListCtrl}).
      when('/add', {templateUrl: 'partials/edit.html', controller: EditCtrl}).
      when('/edit/:id', {templateUrl: 'partials/edit.html', controller: DetailCtrl}).
      when('/:id', {templateUrl: 'partials/detail.html', controller: DetailCtrl}).
      otherwise({redirectTo: '/'});
}]);

/* Controllers */

function ListCtrl($scope, $http, $location) {
	//need a smarter way to do this... required to update after save
	$('#List').bind('refresh', function() {
		$http.get('/index/recipes').success(function(data) {
			$scope.recipes = data;
		});
	}).trigger('refresh');
	$scope.orderProp = 'name';
	$scope.Add = function() {
		$location.path('/add');
	}
}

function DetailCtrl($http, $scope, $routeParams) {
	$scope.id = $routeParams.id;
	$http.get('/index/recipe/' + $scope.id).success(function(data) {
		$scope.recipe = data;
	});
}

function EditCtrl($http, $scope, $location, $routeParams) {
	if ($routeParams.id == undefined) {
		$scope.recipe = { name: "" };
	}
	$scope.Save = function() {
		$http.post('/index/save', $scope.recipe).success(function(data) {
			$('#List').trigger('refresh');
		});
	}
}

